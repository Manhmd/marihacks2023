import os

import openai

openai.api_key = os.getenv("OPENAI_API_KEY")


class GptCharacter:
    
    def __init__(self, name):
        self.chatting = False
        self.name = name
        self.prompts = []
    
    
    def add_prompt(self, prompt):
        
        if type(prompt) == str:
            self.prompts.append(prompt)
            
    def get_response(self, question):
        response = openai.Completion.create(
            model='text-davinci-003',
            prompt=f"""
            Imagine you are a person with the following traits
            You are 20 years old
            You really like cats
            You are introverted
            You HATE dragons
            You HATE tea
            You LOVE ariana grande
            Now answer the following prompt conversationally, assuming you are the person described above:
            {question}
            """,
            temperature=0.7
        )
        
        return response.choices[0].text