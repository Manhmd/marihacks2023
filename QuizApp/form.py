from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired
class Form(FlaskForm):
    text = StringField('text', validators=[DataRequired()])
