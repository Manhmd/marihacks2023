from flask import Flask, request, render_template, jsonify
import secrets 
import os

def create_app():
    app=Flask(__name__)
    app.config.from_mapping(
        SECRET_KEY=secrets.token_urlsafe(32),
    )
    from .home_view import bp as home_bp
    app.register_blueprint(home_bp)

    from .form_view import bp as form_bp
    app.register_blueprint(form_bp)
    

    basedir = os.path.abspath(os.path.dirname(__file__))
    data_file = os.path.join(basedir, 'static/script.js')

    questions = []
    for line in data_file.readlines():
        questions.append(line.rstrip())
        return jsonify(questions)
    return app
