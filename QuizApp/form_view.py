from flask import Blueprint, render_template, request
from .form import Text, TextForm

bp = Blueprint('form', __name__, url_prefix="/form/")

@bp.route('/', methods=['GET', 'POST'])
def form_view():
    form=TextForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            text=form.text.data
        #prompt chatGPT for response here
    elif request.method == 'GET':
        pass
    #return template 

