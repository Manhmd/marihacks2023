questions=[
    {
        "question":"Is your character an introvert, an extrovert or an ambivert?",
        "responses":[{"tag":"extrovert", "prompt":"You are outgoing and enjoy spending lots of time with others. You are an extrovert."},
                     {"tag":"introvert","prompt":"You are reserved and need to spend time alone to recharge. You are an introvert."},
                     {"tag":"ambivert", "prompt":"You have both introverted and extroverted traits. You may find spending time with others energizing one day but tiring he next."}]
    }
]